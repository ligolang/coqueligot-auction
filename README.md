Smart contract written in Coq and extracted to OCaml → CameLIGO → Michelson, with a specification and proof for that specification.

This implements a naive auction contract, where the sole object is initially available, then has a current bid. Bids can be made
when the object is in either state, as long as the new bid is higher than the current bid (if there is a current bid). The auction
ends when the `Claim` entry point is called, at which point no further operations will be accepted.

**This contract does not care about tokens being transferred, does not check if the `Claim` entry point is called after a given date,
and does not check the identity of the source or sender. It is definitely not suitable for use in production, and is only a tech
demo showing how to write a smart contract in Coq and write proofs for it.**

```
less build.sh

# if you're okay with the build script, run it:
./build.sh

# after the dependencies are downloaded,
# the build script can be invoked again with:
./build.sh --no-download
```
