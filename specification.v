Require Import contract.
Require Import Notations.
Require Import List.
Import ListNotations.
Require Import Nat.
Require Import lemma_fold_invariant_aux.

(* ********************************************* *)
(*                 Specification                 *)
(* ********************************************* *)

(* The object is sold for an amount greater than or equal to any (valid) bid seen so far. *)

Definition main_get_storage (s : storage) (p : param) :=
  snd (main (p, s)).

Definition run_multiple_calls_ (s : storage) (l : list param) :=
  fold_left main_get_storage l s.

Definition run_multiple_calls (l : list param) :=
  run_multiple_calls_ initial_storage l.

Fixpoint contains_bid_ (has_bid : bool) (l : list param) (amount : nat) :=
  match l with
  | [] => False
  | Bid a :: tl => if a =? amount then True else contains_bid_ true tl amount
  | Claim :: tl => if has_bid then False else contains_bid_ has_bid tl amount
  end.

Definition contains_bid (l : list param) (amount : nat) :=
   contains_bid_ false l amount.

(* If after some operations, the object is sold for an amount,
   it will be sold for an amount >= any bid.

   In other words, the object is never sold for less than
   the amount of a (valid) bid. *)
Definition spec_bids_are_less_than_sold_for_amount :=
  forall (l : list param) (amount : nat),
    forall (Hcontains : contains_bid l amount),
      match (run_multiple_calls l).(state) with
      | Sold sold_for_amount => 
          amount <= sold_for_amount
      | _ => True
      end.

(* The (current or final) price of the object is the max of all the (valid) bids seen so far *)

Definition valid_op (s : storage) (p : param) :=
  match (p, s.(state)) with
  | (Bid newAmount, Available)                 => true
  | (Bid newAmount, CurrentBid existingAmount) => existingAmount <? newAmount
  | (Claim,         CurrentBid existingAmount) => contract.has_permission_to_claim s.(metadata)
  | (_,             _storage)                  => false
  end.
Definition valid_ops := filter_args valid_op main_get_storage.
Definition valid_ops' lop := valid_ops initial_storage lop.
Fixpoint map_filter {A B} (f : A -> option B) l :=
  match l with
  | [] => []
  | hd :: tl => match f hd with
                | Some x => x :: map_filter f tl
                | None => map_filter f tl
                end
  end.
Definition get_bid (p : param) :=
  match p with
  | Bid n => Some n
  | Claim => None
  end.
Definition get_bids (l : list param) := map_filter get_bid l.
Definition aux_max := nat.
Definition Pmax (s : storage) (a : aux_max) : Prop :=
  (* Pmax: the storage is equal to the max of the bids seen so far *)
  match s.(state) with
  | Available => a = 0
  | CurrentBid n => n = a
  | Sold n => n = a
  end.
Definition spec_max :=
  forall (l : list param),
    Pmax (run_multiple_calls l) (list_max (get_bids (valid_ops' l))).

(* ********************************************* *)
(*              End of Specification             *)
(* ********************************************* *)
