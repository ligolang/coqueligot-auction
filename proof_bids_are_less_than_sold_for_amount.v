Require Import contract.
Require Import specification.
Require Import Notations.
Require Import List.
Import ListNotations.
Require Import Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Lia.
Require Import lemma_fold_invariant_aux.

(* ********************************************* *)
(*                  Proof of spec                *)
(* ********************************************* *)

Definition valid_op_cb (s : storage) (p : param) :=
  match (p, s.(state)) with
  | (Claim, Available) => false
  | (_, Sold _) => false
  | _ => true
  end.
Definition valid_ops_cb := filter_args valid_op_cb main_get_storage.
Definition valid_ops_cb' lop := valid_ops_cb initial_storage lop.

Definition aux_noless : Type := list param.
Definition g_noless : aux_noless -> param -> aux_noless := fun aux arg =>
  aux ++ [arg].
Definition P_noless : storage -> aux_noless -> Prop := fun s aux =>
  s = run_multiple_calls aux /\
  forall (amount : nat),
    forall (Hcontains : In (Bid amount) (valid_ops_cb' aux)),
      match (run_multiple_calls aux).(state) with
      | Sold n => (amount <= n)
      | CurrentBid n => (amount <= n)
      | Available => False (* impossible *)
      end.

Lemma valid_ops_cb_app :
  forall l1 l2 s,
    valid_ops_cb s (l1 ++ l2) =
    valid_ops_cb s l1 ++ valid_ops_cb (run_multiple_calls_ s l1) l2.
intros l1. 
induction l1 as [ | a l' IHl].
- compute; auto.
- intros l2 s.
  unfold valid_ops_cb at 1.
  rewrite <- app_comm_cons.
  unfold filter_args at 1; fold (filter_args valid_op_cb).
  fold valid_ops_cb.
  unfold valid_ops_cb at 3.
  unfold filter_args at 1; fold (filter_args valid_op_cb).
  fold valid_ops_cb.
  case_eq (valid_op_cb s a).
  * intros Hvalid.
    rewrite <- app_comm_cons.
    f_equal.
    apply IHl.
  * intros Hinvalid.
    apply IHl.
Qed.

Lemma run_multiple_calls_app :
  forall l a,
  run_multiple_calls (l ++ [a]) = main_get_storage (run_multiple_calls l) a.
intros.
apply fold_left_app.
Qed.

(* This lemma is named after BLÅHAJ, because this project was started in
   Sweden and a native plushie was adopted around the same time. *)
Lemma Blahaj :
  forall (xarg : param) (xstate : storage) (xaux : aux_noless),
    P_noless xstate xaux ->
    valid_op_cb xstate xarg = true ->
    P_noless (main_get_storage xstate xarg) (g_noless xaux xarg).
intros xarg xstate xaux Hprev Hvalid.
unfold g_noless.
unfold P_noless in Hprev.
destruct Hprev as [Hstate_result Hprev].
unfold P_noless.
constructor; cycle 1.
- intros amt Hin.
pose proof (Hprev' := Hprev amt); clear Hprev.
rewrite run_multiple_calls_app.
set (result := run_multiple_calls xaux) in *.
(*cut (xstate = result); [intros Hstate_result|admit].*)
rewrite Hstate_result in *; clear Hstate_result xstate.
case_eq result; intros state metadata resultEq.
case_eq state.
+ intros Hresult.
  (*rewrite Hstate_result in *; clear Hstate_result xstate.*)
  case_eq xarg.
  * intros n Harg.
    rewrite Harg in Hvalid.
    assert (Hvalid' := Hvalid).
    unfold valid_op in Hvalid.
    unfold main_get_storage.
    unfold main.
    unfold main_option.
    unfold snd at 1.
    unfold valid_ops_cb' in Hin.
    rewrite valid_ops_cb_app in Hin.
    apply in_app_or in Hin.
    fold (valid_ops_cb' xaux) in Hin.
    fold (run_multiple_calls xaux) in Hin.
    destruct Hin as [Hin' | Hin'].
    -- pose proof (Hprev'' := Hprev' Hin'); clear Hprev'.
       rewrite resultEq in Hprev''.
       rewrite Hresult in Hprev''.
       contradiction.
    -- unfold contract.state in *.
       rewrite resultEq in Hprev'.
       rewrite Hresult in Hprev'.
       unfold valid_ops_cb in Hin'.
       unfold filter_args in Hin'.

       rewrite Harg in Hin'.
       subst state.
       unfold valid_op_cb in Hvalid.
       unfold contract.state in Hvalid'.
       unfold result in *;
         clear result;
        set (result := run_multiple_calls xaux) in *.
       rewrite Hvalid' in Hin'.
       unfold In in Hin'.
       destruct Hin' as [Hin' | Hcontra]; [|contradiction].
       injection Hin' as Hin'.
       rewrite Hin'.
       reflexivity.
  * intros Harg.
    rewrite Harg in Hvalid.
    rewrite resultEq in Hvalid.
    rewrite Hresult in Hvalid.
    unfold valid_op_cb in Hvalid.
    inversion Hvalid.
+ intros n Hresult.
  rewrite resultEq in Hvalid.
  (*rewrite Hstate_result in *.*)
  rewrite Hresult in Hvalid.
  case_eq xarg.
  * intros n0 Harg.
    unfold main_get_storage.
    unfold main.
    unfold main_option.
    case_eq (n <? n0).
    -- intros Hless.
      rewrite Harg in Hvalid.
      assert (Hvalid' := Hvalid).
      unfold valid_op_cb in Hvalid.
      unfold snd at 1.
      unfold valid_ops_cb' in Hin.
      rewrite valid_ops_cb_app in Hin.
      apply in_app_or in Hin.
      fold (valid_ops_cb' xaux) in Hin.
      fold (run_multiple_calls xaux) in Hin.
      destruct Hin as [Hin' | Hin'].
      ** pose proof (Hprev'' := Hprev' Hin'); clear Hprev'.
         unfold contract.state in *.
         rewrite resultEq in Hprev''.
         rewrite Hresult in Hprev''.
         rewrite Nat.ltb_lt in Hless.
         lia.
      ** unfold contract.state in *.
         rewrite resultEq in Hprev'.
         unfold valid_ops_cb in Hin'.
         unfold filter_args in Hin'.
         rewrite Harg in Hin'.
         subst result; set (result := run_multiple_calls xaux) in *.
         rewrite resultEq in Hin'.
         rewrite Hresult in Hin'.
         rewrite Hvalid' in Hin'.
         unfold In in Hin'.
         destruct Hin' as [Hin' | Hcontra]; [|contradiction].
         injection Hin' as Hin'.
         rewrite Hin'.
         reflexivity.
    -- intros Hless.
      rewrite Harg in Hvalid.
      unfold valid_op_cb in Hvalid.
      unfold fail_in_main.
      unfold snd.
      unfold valid_ops_cb' in Hin.
      rewrite valid_ops_cb_app in Hin.
      apply in_app_or in Hin.
      fold (valid_ops_cb' xaux) in Hin.
      fold (run_multiple_calls xaux) in Hin.
      destruct Hin as [Hin' | Hin'].
      ** pose proof (Hprev'' := Hprev' Hin'); clear Hprev'.
        rewrite resultEq in *.
        unfold contract.state in *.
        rewrite Hresult in Hprev''.
        assumption.
      ** rewrite resultEq in *.
         unfold contract.state in *.
         rewrite Hresult in Hprev'.
         unfold valid_ops_cb in Hin'.
         unfold filter_args in Hin'.
         rewrite Harg in Hin'.
         subst result; set (result := run_multiple_calls xaux) in *.
         rewrite resultEq in *.
         rewrite Hresult in Hin'.
         case_eq (valid_op_cb result (Bid n0)).
         *** intros Ha.
             rewrite resultEq in Ha.
             rewrite Hresult in Ha.
             rewrite Ha in Hin'.
             unfold In in Hin'.
             destruct Hin' as [n0amt|contra]; try contradiction.
             inversion n0amt as [H0].
             rewrite H0 in *.
             apply Nat.ltb_nlt in Hless.
             lia.
         *** intros Ha.
             rewrite resultEq in Ha.
             rewrite Hresult in Ha.
             rewrite Ha in Hin'.
             unfold In in Hin'.
             contradiction.
(*         rewrite Hvalid' in Hin'.
         unfold In in Hin'.
         destruct Hin' as [Hin' | Hcontra]; [|contradiction].
         injection Hin' as Hin'.
         rewrite Hin'.
         reflexivity.

      admit.*)
(*
      rewrite Hless in Hvalid.
      inversion Hvalid.
*)
  * intros Harg.
    rewrite Harg in Hvalid.
    assert (Hvalid' := Hvalid).
    unfold valid_op_cb in Hvalid.
    unfold main_get_storage.
    unfold main.
    unfold main_option.
    unfold snd at 1.
      unfold valid_ops_cb' in Hin.
      rewrite valid_ops_cb_app in Hin.
      apply in_app_or in Hin.
      fold (valid_ops_cb' xaux) in Hin.
      fold (run_multiple_calls xaux) in Hin.
      destruct Hin as [Hin' | Hin'].
      ** pose proof (Hprev'' := Hprev' Hin'); clear Hprev'.
        unfold contract.state in *.
        rewrite resultEq in *.
        rewrite Hresult in Hprev''.
        case_eq (has_permission_to_claim metadata).
        *** lia.
        *** intros permission_ok.
            unfold fail_in_main.
            lia.
      ** unfold contract.state in *.
         rewrite resultEq in Hprev'.
         rewrite <- Hresult in Hvalid'.
         rewrite <- resultEq in Hvalid'.
         rewrite Hresult in Hprev'.
         unfold valid_ops_cb in Hin'.
         unfold filter_args in Hin'.
         rewrite Harg in Hin'.
         subst result; set (result := run_multiple_calls xaux) in *.
         rewrite Hvalid' in Hin'.
         unfold In in Hin'.
         destruct Hin' as [Hin' | Hcontra]; [|contradiction].
         inversion Hin'.
  + intros n Hresult.
    rewrite resultEq in Hvalid.
    rewrite Hresult in Hvalid.
    unfold valid_op_cb in Hvalid.
    case_eq xarg.
    * intros * Harg;
      rewrite Harg in Hvalid;
      inversion Hvalid.
    * intros * Harg;
      rewrite Harg in Hvalid;
      inversion Hvalid.
- unfold run_multiple_calls.
  unfold run_multiple_calls_.
  rewrite fold_left_app.
  unfold fold_left at 1.
  fold (run_multiple_calls_ initial_storage xaux).
  fold (run_multiple_calls xaux).
  rewrite Hstate_result.
  reflexivity.
Qed.

Theorem invalid_cb_is_noop_on_main :
  forall storage x,
    valid_op_cb storage x = false -> main_get_storage storage x = storage.
intros s x Hinvalid.
unfold main_get_storage.
unfold main.
induction s as [state metadata].
case_eq x.
- case_eq state.
  + intros Hs n Hx.
    rewrite Hs in Hinvalid.
    rewrite Hx in Hinvalid.
    unfold valid_op_cb in Hinvalid.
    inversion Hinvalid.
  + intros n Hs n0 Hx.
    rewrite Hs in Hinvalid.
    rewrite Hx in Hinvalid.
    unfold valid_op_cb in Hinvalid.
    case_eq (n <? n0).
    * intros Hless.
      inversion Hinvalid.
    * intros Hmore.
      unfold main_option.
      rewrite Hmore.
      reflexivity.
  + intros n Hs n0 Hx.
    reflexivity.
- case_eq state.
  + intros Hs Hx.
    reflexivity.
  + intros n Hs Hx.
    rewrite Hs in Hinvalid.
    rewrite Hx in Hinvalid.
    unfold valid_op in Hinvalid.
    inversion Hinvalid.
  + intros n Hs Hx.
    reflexivity.
Qed.

Lemma Blahaj' :
  forall l,
    P_noless (run_multiple_calls l) (fold_left g_noless (valid_ops_cb' l) []).
intros l.
unfold run_multiple_calls.
unfold run_multiple_calls_.
unfold valid_ops_cb'.
unfold valid_ops_cb.
apply fold_invariant_aux.
- apply invalid_cb_is_noop_on_main.
- apply Blahaj.
- compute; auto.
Qed.

Lemma fold_g_noless_identity :
  forall l, (fold_left g_noless l []) = l.
cut (forall lb la lc, la ++ lb = lc -> (fold_left g_noless lb la) = lc); auto.
intros lb.
induction lb as [|x lb IHl].
- intros la lc Heq.
  rewrite app_nil_r in Heq.
  rewrite Heq.
  compute; auto.
- intros la lc Heq.
  unfold fold_left; fold (fold_left g_noless).
  unfold g_noless at 2.
  apply IHl.
  rewrite <- Heq.
  rewrite app_assoc_reverse.
  f_equal.
Qed.

Lemma valid_ops_cb'_idempotent :
  forall l, (valid_ops_cb' (valid_ops_cb' l)) = (valid_ops_cb' l).

cut (forall l (Heq : initial_storage = initial_storage),
       (valid_ops_cb' (valid_ops_cb' l)) = (valid_ops_cb' l)).
auto.

intros l.
unfold valid_ops_cb'.
generalize initial_storage at 2 3 as s.
generalize initial_storage as s'.
induction l as [ | a l' IHl].
- compute; auto.
- intros s' s Heq.
  unfold valid_ops_cb.
  unfold filter_args.
  repeat fold (filter_args valid_op_cb).
  repeat fold valid_ops_cb.
  case_eq (valid_op_cb s' a).
  * intros Hvalid.
    unfold valid_ops_cb at 1.
    unfold filter_args at 1.
    fold (filter_args valid_op_cb).
    rewrite Heq in *.
    rewrite Hvalid.
    fold valid_ops_cb.
    f_equal.
    apply IHl.
    reflexivity.
  * intros Hinvalid.
    apply IHl.
    rewrite invalid_cb_is_noop_on_main; assumption.
Qed.

Definition is_sold storage := match storage with
| Sold _ => True
| _ => False
end.

Lemma contains_bid_in :
  forall l amount,
    contains_bid l amount -> In (Bid amount) (valid_ops_cb' l).
unfold contains_bid.
unfold valid_ops_cb'.
set (seen_bid := false).
set (storage := initial_storage).
case_eq storage; intros initial_state initial_metadata initial_storage_eq.
assert (initial_state = Available <-> seen_bid = false);
  [solve [inversion initial_storage_eq; compute; auto]|].
intros l.
assert (is_sold storage.(state) -> seen_bid = false) as Hsold;
  [contradiction|].
assert (initial_state = storage.(state)) as fold_initial_state;
 [rewrite initial_storage_eq; unfold contract.state; auto|].
rewrite fold_initial_state in H.
rewrite <- initial_storage_eq in *.
clear fold_initial_state initial_storage_eq initial_state initial_metadata.
generalize dependent Hsold.
generalize dependent seen_bid.
generalize dependent storage.
induction l; [compute; auto|].
intros storage seen_bid Hrelation Hsold amount.
unfold valid_ops_cb.
unfold filter_args; fold (filter_args valid_op_cb).
case_eq (valid_op_cb storage a).
+ intros Hvalid.
  case_eq a.
  * intros n Ha.
    unfold contains_bid_; fold contains_bid_.
    case_eq (n =? amount).
    ** intros Heq _.
       apply Nat.eqb_eq in Heq.
       rewrite Heq.
       apply in_eq.
    ** intros Hneq Hcontains.
       apply Nat.eqb_neq in Hneq.
       apply in_cons.
       fold (valid_ops_cb).
       apply (IHl _ true); try assumption.
       unfold main_get_storage.
       unfold main.
       unfold main_option.
       induction storage as [state metadata].
       case_eq state. 
       -- intros Hstate.
          unfold snd.
          split; intros Hcontra; inversion Hcontra.
       -- intros n0 Hstorage.
          case_eq (n0 <? n).
          --- intros Hless.
              unfold snd.
              split; intros Hcontra; inversion Hcontra.
          --- intros Hless.
              unfold fail_in_main.
              unfold snd.
              split; intros Hcontra; inversion Hcontra.
       -- intros n0 Hstorage.
          unfold fail_in_main.
          unfold snd.
          split; intros Hcontra; inversion Hcontra.
       -- induction storage as [state metadata].
          case_eq state.
          --- unfold main_get_storage.
              unfold main.
              unfold main_option.
              unfold snd.
              unfold is_sold.
              contradiction.
          --- intros n0 Hstorage.
              unfold main_get_storage.
              unfold main.
              unfold main_option.
              case_eq (n0 <? n);
                unfold snd;
                unfold is_sold;
                contradiction.
          --- intros n0 Hstorage.
              rewrite Hstorage in Hsold.
              unfold is_sold in Hsold.
              unfold contract.state in *.
              rewrite Hsold in Hrelation; auto.
              rewrite Hstorage in Hrelation.
              inversion Hrelation as [H H'].
              assert (Sold n0 = Available) as Hcontra; [auto|].
              inversion Hcontra.
  * intros Ha Hcontains.
    apply in_cons.
    generalize dependent Hcontains.
    unfold contains_bid_; fold contains_bid_.
    case_eq seen_bid; try contradiction.
    intros Hnot_seen.
    fold valid_ops_cb.
    apply IHl.
    unfold main_get_storage.
    unfold main.
    unfold main_option.
    induction storage as [state metadata].
    case_eq state.
    -- compute; auto.
    -- intros n0 Hstorage.
       unfold contract.state in *.
       rewrite Hnot_seen in Hrelation.
       inversion Hrelation as [H H'].
       assert (state = Available) as Hcontra; auto.
       rewrite Hstorage in Hcontra.
       inversion Hcontra.
    -- intros n Hstorage.
       rewrite Hnot_seen in Hrelation.
       inversion Hrelation as [H H'].
       assert (state = Available) as Hcontra; auto.
       rewrite Hstorage in Hcontra.
       inversion Hcontra.
    -- auto.
+ intros Hinvalid.
  fold valid_ops_cb.

unfold contains_bid_; fold contains_bid_.


  case_eq a.
  * intros n Ha.
    (*case_eq (n =? amount).*)
      (*rewrite Nat.eqb_eq in Heq.
      rewrite Heq.*)
    rewrite Ha in Hinvalid.
    unfold valid_op_cb in Hinvalid.
    induction storage as [state metadata].
    case_eq state.
    -- intros Hstorage.
       rewrite Hstorage in Hinvalid; inversion Hinvalid.
    -- intros n0 Hstorage.
       rewrite Hstorage in Hinvalid; inversion Hinvalid.
    -- intros n0 Hstorage.
       rewrite Hstorage in Hinvalid.
       rewrite Hstorage in Hrelation.
       inversion Hrelation as [H H'].
       rewrite Hstorage in Hsold.
       pose proof (Hcontra := H' (Hsold ltac:(compute; auto))).
       inversion Hcontra.
  * intros Ha.
    rewrite Ha in Hinvalid.
    unfold valid_op_cb in Hinvalid.
    induction storage as [state metadata].
    case_eq state.
    -- intros Hstorage.
       rewrite Hstorage in Hinvalid.
       rewrite Hstorage in Hrelation.
       assert (seen_bid = false) as H; [apply Hrelation; reflexivity|].
       rewrite H.
       rewrite H in Hrelation.
       unfold main_get_storage.
       unfold main.
       unfold main_option.
       unfold fail_in_main.
       unfold snd.
       apply IHl; try assumption.
       auto.
    -- intros n0 Hstorage.
       rewrite Hstorage in Hinvalid; inversion Hinvalid.
    -- intros n0 Hstorage.
       rewrite Hstorage in Hinvalid.
       rewrite Hstorage in Hrelation.
       inversion Hrelation as [H H'].
       rewrite Hstorage in Hsold.
       pose proof (Hcontra := H' (Hsold ltac:(compute; auto))).
       inversion Hcontra.
Qed.

Lemma filter_valid_ops_cb_short_proof :
  forall (l : list param) (s : storage),
    run_multiple_calls_ s l =
    run_multiple_calls_ s (valid_ops_cb s l).

intros l.
induction l as [| hd tl IHl].
- auto.
- intro s.
  case_eq hd.
  * intros n Hhd.
    induction s as [state metadata].
    case_eq state;
      intros n0;
      intros;
      try apply IHl.
      (*
      case_eq (n0 <? n)
         intros HH;
         unfold valid_ops;
         unfold filter_args;
         unfold valid_op;
         unfold run_multiple_calls_;
         unfold fold_left;
         unfold main_get_storage;
         unfold main;
         unfold main_option;
         rewrite HH;
         try rewrite HH;
         apply IHl.*)
  * intros.
    induction s as [state metadata].
    case_eq state;
      intros;
      apply IHl.
Qed.

Theorem filter_valid_ops_cb' :
  forall (l : list param),
    run_multiple_calls l =
    run_multiple_calls (valid_ops_cb' l).
intros.
unfold run_multiple_calls.
unfold valid_ops_cb.
apply filter_valid_ops_cb_short_proof.
Qed.

Theorem unit_test_bids_are_less_than_sold_for_amount :
  spec_bids_are_less_than_sold_for_amount.
unfold spec_bids_are_less_than_sold_for_amount.
intros l amount Hcontains.
pose proof (Blahaj' l) as Hlp.
unfold P_noless in Hlp.
destruct Hlp as [Hlp1 Hlp2].
clear Hlp1.

rewrite fold_g_noless_identity in Hlp2.
rewrite valid_ops_cb'_idempotent in Hlp2.
apply contains_bid_in in Hcontains.
apply Hlp2 in Hcontains.
rewrite <- filter_valid_ops_cb' in Hcontains.

set (result := (run_multiple_calls l)) in *.
induction result as [result_state result_metadata].
case_eq result_state; try (solve [unfold state; intros; auto]).
intros n Hresult.
rewrite Hresult in Hcontains.
assumption.
Qed.

(* ********************************************* *)
(*                  End of Proof                 *)
(* ********************************************* *)
