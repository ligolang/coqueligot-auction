(* Simple auction contract *)

(* Require Coq built-ins, declare LIGO built-ins and shorthands *)
Require Import Notations.
Require Import List.
Require Import Nat.
Import ListNotations.
Notation "()" := tt.                (* tt means "null" or "Unit" in Coq *)
Parameter tez : Type.
Parameter operation : Type.             (* declare external LIGO type "operation" *)
(*
Definition operation : Type.        (* type operation = … *)
(* could substitute with an actual implementation of operation as e.g. a string of bytes. *)
exact (list nat).
Qed. (* make it opaque. *)
*)
Parameter contract : Type -> Type.
Parameter destination_account : contract unit.
Parameter transaction : forall {param : Type} (action : param) (amount : tez), contract param -> operation.
Parameter lit_mutez : nat -> tez.

(* Declare external LIGO function "fail_in_main". Note that it should only be
   used once at the top-level of the "main" wrapper function. This is due to the
   fact that Coq does not support exceptions or partial functions, so it must be
   absolutely clear that the behaviour of failure is identical to a no-op contract.
   See the explanation below in the section about extraction. *)
Definition fail_in_main {A} (a : A) := a.

(* ********************************************* *)
(*                    Contract                   *)
(* ********************************************* *)

(* Declare enum for the storage (three states) *)
Inductive storage_state :=
| Available  : storage_state
| CurrentBid : nat -> storage_state
| Sold       : nat -> storage_state.

Record t_metadata := {
  end_date : nat
}.

Record storage := {
  state : storage_state;
  metadata : t_metadata
}.

(* Declare enum for the parameter (two possible actions) *)
Inductive param :=
| Bid   : nat -> param
| Claim : param.

(* TODO: make this the current date. (run_multiple_calls will need a source of unknown values (e.g. delays between blocks)) *)
Definition Tezos_get_time := 999.

Definition has_permission_to_claim (metadata : t_metadata) : bool :=
  (* dummy placeholder check *)
  Nat.ltb metadata.(end_date) Tezos_get_time.

(* Behaviour of the contract. *)
Definition main_option (ps : param * storage) : option ((list operation) * storage) :=
  match ps with

  (* Start a bid *)
  | (Bid newAmount, {| state := Available; metadata := metadata |}) =>
    Some ([], {| state := CurrentBid newAmount; metadata := metadata |})

  (* New bid (must be higher than the existing *)
  | (Bid newAmount, {| state := CurrentBid existingAmount; metadata := metadata |})  =>
    if existingAmount <? newAmount then
      Some ([], {| state := CurrentBid newAmount; metadata := metadata |})
    else
      None

  (* Finish the auction *)
  | (Claim, {| state := CurrentBid existingAmount; metadata := metadata |}) =>
    if has_permission_to_claim metadata then
      Some ([transaction tt (lit_mutez 0) destination_account],
            {| state := Sold existingAmount; metadata := metadata |})
    else
      None
    (* All other cases are errors. *)
  | (_,             _storage)                  =>
    None
  end.

Definition main (ps : param * storage) : (list operation * storage) :=
  match main_option ps with
  (* On success, return the result *)
  | Some result => result
  (* On failure, return an empty list of operations and the unmodified storage *)
  | None => fail_in_main ([], let (p,s) := ps in s)
  end.

Definition initial_storage : storage := {|
  state := Available;
  metadata := {|
    end_date := 123
  |}
|}.

(* ********************************************* *)
(*                End of Contract                *)
(* ********************************************* *)