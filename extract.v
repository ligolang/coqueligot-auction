Require Import contract.
Require Import specification.
Require Import proof_max.
Require Import proof_bids_are_less_than_sold_for_amount.
Require Import List.
Import ListNotations.
Require Import Nat.

(* Extract to OCaml language *)
Require Extraction.
Extraction Language OCaml.

(* LIGO built-ins *)
Extract Inlined Constant tez => "tez".
Extract Inlined Constant operation => "operation".
Extract Constant contract "'t" => "'t contract".
Extract Inlined Constant destination_account => "(Tezos.get_contract_with_error (Tezos.get_sender ()) ""oops"" : unit contract)".
Extract Inlined Constant transaction => "Tezos.transaction".
Extract Inlined Constant lit_mutez => "(fun n -> mutez_of_nat n)".

(* OCaml built-ins *)
Extract Inductive unit => unit ["()"].
(* Implement external LIGO function "fail" *)
(*Extraction Implicit fail [A a].*)
(* It is okay for some proofs to implement our identity function named "fail" with
   "failwith", even in the case where the code isn't
   "let main = function … | … -> fail | …" but e.g. is
   "let main = function … | … -> f (fail) | …".

   In the coq world, the extra f(identity(some_value)) in the chain of calls
   still needs to satisfy the proofs. This is okay for some proofs, but not e.g.
   for proofs saying that a user is always able to claim an auction after some
   time (the action of claiming an auction could go through a "fail" path, which
   would make it seem possible in Coq but wouldn't actually be possible on-chain).

   We therefore use fail_in_main only once, at the top-level, where it is obvious
   that its return value is never used by the rest of the contract.

   If the following line is removed, the resulting contract will behave exactly
   the same way, but will consume gas and perform a no-op instead of warning the
   user before performing invalid operations.
 *)
Definition ok_main (ps : param * storage) : (list operation * storage) :=
  match ps with
  (* Start a bid *)
  | (Bid newAmount, {| state := Available ; metadata := metadata |}) =>
    ([], {| state := CurrentBid newAmount ; metadata := metadata |})
  (* e.g. rest is not implemented yet *)
  | (_p,s) =>
    fail_in_main ([], s)
  end.
Definition bad_f (x : list operation * storage) := (fst x, {| state := CurrentBid 42 ; metadata := (snd x).(metadata) |}).
Definition bad_main (ps : param * storage) : (list operation * storage) :=
  match ps with
  (* Start a bid *)
  | (Bid newAmount, {| state := Available ; metadata := metadata |}) =>
    ([], {| state := CurrentBid newAmount ; metadata := metadata |})
  | (_p,s)                     => bad_f (fail_in_main ([], s))
  end.
Extract Inlined Constant fail_in_main => "(fun (_ : (operation list * storage)) : (operation list * storage) -> failwith ""Err"")".
(*Unset Extraction SafeImplicits.*)
Extract Inductive list => "list" [ "[]" "(::)" ].
Extract Inductive prod => "(*)"  [ "(,)" ].
Extract Inductive option => "option"  [ "Some" "None" ].
Extract Inductive nat => nat [ "(abs 0)" "(fun (x : nat) : nat -> x + (abs 1))" ] "(fun fO fS n -> if n=0 then fO () else fS (n-1))".
Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inlined Constant ltb => "(fun (a : nat) (b : nat) : bool -> a < b)".
Extract Inlined Constant eqb => "(fun (a : nat) (b : nat) : bool -> a = b)".

(* Prevent access to opaque definitions. *)
Unset Extraction AccessOpaque.

(* Compile Coq code to OCaml *)
Print Assumptions main.
Print Assumptions unit_test_max.
Print Assumptions unit_test_bids_are_less_than_sold_for_amount.
(* TODO: Print Assumptions test_blah … . *)
Extraction "contract.1.ocaml.ml" main.
