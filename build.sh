#!/usr/bin/env bash

set -euET -o pipefail

if test $# -lt 1 || test "x$1" != "x--no-download"; then
  if test -e ligo || test -e ocaml-to-ligo; then
    printf %s\\n 'Please remove the following files and directories if they exist: ligo ocaml-to-ligo'
    printf %s\\033'[1;37m'%s\\033'[m'\\n '  or pass the flag ' '--no-download'
    exit 1
  fi

  # Download LIGO
  wget -c https://gitlab.com/ligolang/ligo/-/jobs/5419828800/artifacts/raw/ligo -O ligo_
  mv -i ligo_ ligo
  chmod +x ./ligo

  # Download OCaml-to-LIGO
  (
    git clone https://github.com/marigold-dev/ocaml-to-ligo.git
    cd ocaml-to-ligo
    git checkout aee3e77199f9910b675a94c96c9cbe59ec4c416d
    git apply ../ocaml-to-ligo.patch
    npm i esy
  )
fi

printf %s\\n '=========================================='
printf %s\\n 'Compiling OCaml2LIGO'
printf %s\\n '=========================================='

(
  cd ocaml-to-ligo
  ./node_modules/esy/bin/esy
  ./node_modules/esy/bin/esy build
  ./node_modules/esy/bin/esy dune build
)

if test -e contract.mligo.ml; then rm -f contract.mligo.ml; fi

printf %s\\n '=========================================='
printf %s\\n 'Checking proofs and compiling Coq to OCaml'
printf %s\\n '=========================================='
for i in contract.v lemma_fold_invariant_aux.v specification.v proof_max.v proof_bids_are_less_than_sold_for_amount.v extract.v main.v; do coqc $i; done
coqc contract.v

printf %s\\n '=========================================='
printf %s\\n 'Compiling OCaml to LIGO'
printf %s\\n '=========================================='
cp contract.1.ocaml.ml contract.2.maintype.ml
sed -i -e 's/^let main_option =/let main_option : (param*storage) -> (operation list*storage) option =/' contract.2.maintype.ml
sed -i -e 's/^type '\''t contract = '\''t contract$//' contract.2.maintype.ml # hack hack hack
(cd ./ocaml-to-ligo; ./node_modules/esy/bin/esy dune exec O2L -- ../contract.2.maintype.ml) \
| sed -e 's/Y/YB/g; s/X/YA/g' | tr \\n X | sed -e 's/X\[@@@[^]]*\]X/X/g' | tr X \\n | sed -e 's/YA/X/g; s/YB/Y/g' > contract.3.mligo
sed -i -e '1i let mutez_of_nat (n : nat) : tez = (n * 1mutez)' contract.3.mligo

printf %s\\n '=========================================='
printf %s\\n 'Compiling LIGO to Michelson'
printf %s\\n '=========================================='
./ligo compile contract -m Proxy contract.proxy.mligo > contract.4.michelson
cat contract.4.michelson
