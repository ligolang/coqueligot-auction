#import "contract.3.mligo" "C"

module Proxy = struct

  [@entry]
  let proxy (p : C.param) (s : C.storage) : operation list * C.storage =
    C.main (p, s)

end
