Require Import contract.
Require Import specification.
Require Import Notations.
Require Import List.
Import ListNotations.
Require Import Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Lia.
Require Import lemma_fold_invariant_aux.

Definition sold_for (s : storage) (l : list param) :=
  match (run_multiple_calls_ s l).(state) with
  | Available    => 0
  | CurrentBid n => n
  | Sold       n => n
  end.

Theorem invalid_is_noop_on_main :
  forall storage x,
    valid_op storage x = false -> main_get_storage storage x = storage.
intros s x Hinvalid.
unfold main_get_storage.
unfold main.
induction s as [state metadata].
case_eq x.
- case_eq state.
  + intros Hs n Hx.
    rewrite Hs in Hinvalid.
    rewrite Hx in Hinvalid.
    unfold valid_op in Hinvalid.
    inversion Hinvalid.
  + intros n Hs n0 Hx.
    rewrite Hs in Hinvalid.
    rewrite Hx in Hinvalid.
    unfold valid_op in Hinvalid.
    unfold contract.state in Hinvalid.
    case_eq (n <? n0).
    * intros Hless.
      rewrite Hless in Hinvalid.
      inversion Hinvalid.
    * intros Hmore.
      unfold main_option.
      rewrite Hmore.
      reflexivity.
  + intros n Hs n0 Hx.
    reflexivity.
- case_eq state.
  + intros Hs Hx.
    reflexivity.
  + intros n Hs Hx.
    case_eq (contract.has_permission_to_claim metadata).
    * intros permission_ok.
      rewrite Hs in Hinvalid.
      rewrite Hx in Hinvalid.
      unfold valid_op in Hinvalid.
      unfold contract.metadata in Hinvalid.
      rewrite permission_ok in Hinvalid.
      inversion Hinvalid.
    * intros permission_not_ok.
      unfold main_option.
      rewrite permission_not_ok.
      reflexivity.
  + intros n Hs Hx.
    reflexivity.
Qed.

Theorem filter_valid_ops :
  forall (l : list param) (s : storage),
    run_multiple_calls_ s l =
    run_multiple_calls_ s (valid_ops s l).
intros l.
induction l as [| hd tl IHl].
- intro s; induction s as [state metadata].
  auto.
- intro s; induction s as [state metadata].
  case_eq hd.
  * intros n Hhd.
    case_eq state.
    + intro Hs.
      unfold valid_ops.
      fold valid_ops.
      unfold valid_op.
      simpl.
      apply IHl.
    + intro Hs.
      case_eq (Hs <? n).
      -- intros HH XX.
         unfold valid_ops.
         unfold filter_args; fold (filter_args valid_op).
         unfold valid_op.
         unfold main.
         unfold main_option.
         unfold contract.state.
         rewrite HH.
         simpl snd.
         unfold run_multiple_calls_.
         unfold fold_left.
         unfold main_get_storage.
         unfold main.
         unfold main_option.
         rewrite HH.
         simpl snd.
         apply IHl.
      -- intros HH XX.
         unfold valid_ops.
         unfold filter_args; fold (filter_args valid_op).
         unfold main.
         unfold main_option.
         unfold valid_op.
         unfold contract.state.
         rewrite HH.
         simpl snd.
         unfold run_multiple_calls_.
         unfold fold_left.
         unfold main_get_storage.
         unfold main.
         unfold main_option.
         rewrite HH.
         simpl snd.
         apply IHl.
    + intros n0 Hs.
      unfold valid_ops.
      fold valid_ops.
      unfold valid_op.
      simpl.
      apply IHl.
  * intros Hhd.
    unfold valid_ops.
    fold valid_ops.
    unfold valid_op.
    case_eq state.
    + intros Hs.
      apply IHl.
    + intros n Hs.
      case_eq (contract.has_permission_to_claim metadata).
      ++ intros permission_ok.
         set (foo := {| state := CurrentBid n; metadata := metadata |}).

         unfold run_multiple_calls_ at 1.
         unfold fold_left; fold (fold_left main_get_storage).
         unfold foo at 1.
         unfold main_get_storage at 2.
         unfold main.
         unfold main_option.
         rewrite permission_ok.
         unfold snd.
         fold (run_multiple_calls_ {| state := Sold n; metadata := metadata |} tl).

         unfold run_multiple_calls_ at 2.
         unfold valid_ops.
         unfold filter_args; fold (filter_args valid_op).
         unfold valid_op at 1.
         unfold foo at 1.
         unfold contract.state.
         unfold foo at 1.
         unfold contract.metadata.
         rewrite permission_ok.
         fold valid_ops.
         fold (run_multiple_calls_ foo (Claim :: valid_ops (main_get_storage foo Claim) tl)).

         unfold foo at 2.
         unfold main_get_storage.
         unfold main.
         unfold main_option.
         rewrite permission_ok.
         unfold snd.
         set (bar := {| state := Sold n; metadata := metadata |}).
         unfold run_multiple_calls_ at 2.
         unfold fold_left; fold (fold_left main_get_storage).
         unfold main_get_storage at 2.
         unfold main.
         unfold main_option.
         unfold foo.
         rewrite permission_ok.
         unfold snd.
         fold bar.
         fold (run_multiple_calls_ bar (valid_ops bar tl)).
         apply IHl.
      ++ intros permission_not_ok.
         set (foo := {| state := CurrentBid n; metadata := metadata |}).

        unfold run_multiple_calls_ at 1.
        unfold fold_left; fold (fold_left main_get_storage).
        unfold foo at 1.
        unfold main_get_storage at 2.
        unfold main.
        unfold main_option.
        rewrite permission_not_ok.
        unfold fail_in_main.
        unfold snd.
        fold (run_multiple_calls_ {| state := CurrentBid n; metadata := metadata |} tl).

        unfold run_multiple_calls_ at 2.
        unfold valid_ops.
        unfold filter_args; fold (filter_args valid_op).
        unfold valid_op at 1.
        unfold foo at 1.
        unfold contract.state.
        unfold foo at 1.
        unfold contract.metadata.
        rewrite permission_not_ok.
        fold valid_ops.
        fold (run_multiple_calls_ foo (valid_ops (main_get_storage foo Claim) tl)).

        unfold foo at 2.
        unfold main_get_storage.
        unfold main.
        unfold main_option.
        rewrite permission_not_ok.
        unfold fail_in_main.
        unfold snd.
        
        fold foo.
        apply IHl.
    + intros n Hs.
      apply IHl.
Qed.

(*Lemma filter_valid_ops_short_proof :
  forall (l : list param) (s : storage),
    run_multiple_calls_ s l =
    run_multiple_calls_ s (valid_ops s l).

intros l.
induction l as [| hd tl IHl].
- auto.
- intro s.
  induction s as [state metadata].
  case_eq hd.
  * intros n Hhd.
    case_eq state;
      intros n0;
      intros;
      try apply IHl.
      case_eq (n0 <? n);
         intros HH;
         unfold valid_ops;
         unfold filter_args;
         unfold valid_op;
         unfold run_multiple_calls_;
         unfold fold_left;
         unfold main_get_storage;
         unfold main;
         unfold main_option;
         unfold contract.state;
         rewrite HH;
         try rewrite HH;
         apply IHl.
  * intros.
    case_eq state;
      intros;
      apply IHl.
Qed.*)

Theorem filter_valid_ops' :
  forall (l : list param),
    run_multiple_calls l =
    run_multiple_calls (valid_ops' l).
intros.
unfold run_multiple_calls.
unfold valid_ops.
apply filter_valid_ops.
Qed.

Lemma first_op_is_bid'' :
  forall (l : list param),
    match (valid_ops initial_storage l) with
    | Bid n :: _ => True
    | [] => True
    | _ => False
    end.
intros l.
induction l as [| p l IHl].
- now compute.
- case_eq p.
  * intros n Hp.
    unfold initial_storage.
    unfold valid_ops.
    unfold filter_args; fold (filter_args valid_op).
    unfold valid_op.
    unfold contract.state.
    auto.
  * compute.
    auto.
Qed.

Lemma first_op_is_bid_eqv :
  forall (l : list param),
    (valid_ops initial_storage l) <> [] ->
    (match (valid_ops initial_storage l) with
     | Bid n :: _ => True
     | _ => False
     end)
    = (match (valid_ops initial_storage l) with
       | Bid n :: _ => True
       | [] => True
       | _ => False
       end).
intros l nonempty.
set (filtered := valid_ops initial_storage l) in *.
case_eq filtered.
- contradiction.
- auto.
Qed.

Theorem first_op_is_bid :
  forall (l : list param),
    (valid_ops initial_storage l) <> [] ->
    match (valid_ops initial_storage l) with
     | Bid n :: _ => True
     | _ => False
     end.
intros l nonempty.
set (H := first_op_is_bid_eqv l nonempty).
rewrite H.
apply first_op_is_bid''.
Qed.

Fixpoint max_bid (s : storage) (l : list param) :=
  match l with
  | [] => s
  | Bid n :: tl =>
    match s with
    | {| state := Available ; metadata := metadata |} =>
      max_bid {| state := CurrentBid n ; metadata := metadata |} tl
    | {| state := CurrentBid max ; metadata := metadata |} =>
      if n <? max
      then max_bid {| state := CurrentBid max ; metadata := metadata |} tl
      else max_bid {| state := CurrentBid n ; metadata := metadata |} tl
    | {| state := Sold n ; metadata := metadata |} =>
      (* TODO: impossible with assumption valid_ops *)
      {| state := Sold n ; metadata := metadata |}
    end
  | hd :: tl => max_bid s tl
  end.

(*
Compute fold_left u_max                      [ Bid 1; Bid 2; Bid 3; Claim ] initial_max.
Compute fold_left u_max                      [ Bid 1; Bid 2; Bid 3; Claim; Bid 4 ] initial_max.
Compute fold_left u_max (valid_ops Available [ Bid 1; Bid 2; Bid 3; Claim; Bid 4 ]) initial_max.
*)

Definition u_max (a : nat) (p : param) :=
  match p with
  | Bid n => max n a
  | Claim => a
  end.

Lemma fold_get_bids :
  forall l z,
    (fold_left max (get_bids (valid_ops' l)) z)
  = (fold_left u_max (valid_ops' l) z).
intros l.
induction (valid_ops' l) as [| a ll IHll]; [ compute; auto | ].
intros z0.
unfold get_bids.
unfold map_filter; fold (map_filter get_bid).
case_eq a.
- intros n Ha.
  unfold get_bid at 1.
  fold ([n] ++ (map_filter get_bid ll)).
  rewrite fold_left_app.
  fold (get_bids ll).
  fold ([Bid n] ++ ll).
  rewrite fold_left_app.
  replace (fold_left u_max [Bid n] z0) with (fold_left max [n] z0).
  + apply IHll.
  + apply Nat.max_comm.
- intros Ha.
  unfold get_bid at 1.
  fold ([Claim] ++ ll).
  rewrite fold_left_app.
  fold (get_bids ll).
  replace (fold_left u_max [Claim] z0) with z0.
  + apply IHll.
  + reflexivity.
Qed.

Lemma main_preserves_invariant_max :
  forall (xarg : param)
         (xstorage : storage)
         (xaux : nat)
         (Pprev : Pmax xstorage xaux)
         (Hvalid : valid_op xstorage xarg = true),
    Pmax (main_get_storage xstorage xarg) (u_max xaux xarg).
intros xarg xstorage xaux Pprev Hvalid.
induction xstorage as [ xstate metadata ].
case_eq xarg.
- case_eq xstate.
  * intros Hstate n Harg.
    cut (xaux = 0).
    + intros Haux.
      unfold u_max.
      rewrite Haux.
      rewrite Nat.max_0_r.
      compute.
      auto.
    + unfold Pmax in Pprev.
      rewrite Hstate in Pprev.
      assumption.
  * intros n Hstate n0 Harg.
    rewrite Hstate in Hvalid.
    rewrite Harg in Hvalid.
    unfold valid_op in Hvalid.
    unfold Pmax in Pprev.
    rewrite Hstate in Pprev.
    unfold main_get_storage.
    unfold u_max.
    unfold main.
    unfold main_option.
    unfold contract.state in Hvalid.
    rewrite Hvalid.
    rewrite <- Pprev.
    unfold snd.
    unfold Pmax.
    rewrite Nat.max_comm.
    rewrite Nat.max_r.
    + reflexivity.
    + apply Nat.leb_le in Hvalid.
      lia.
  * intros n Hstate n0 Harg.
    rewrite Hstate in Hvalid.
    rewrite Harg in Hvalid.
    unfold valid_op in Hvalid.
    inversion Hvalid.
- case_eq xstate.
  * intros Hstate Harg.
    rewrite Hstate, Harg in Hvalid.
    unfold valid_op in Hvalid.
    inversion Hvalid.
  * intros n Hstate Harg.
    unfold main_get_storage.
    unfold u_max.
    unfold main.
    unfold main_option.
    unfold snd.
    unfold Pmax.
    rewrite Hstate in Pprev.
    unfold Pmax in Pprev.
    rewrite Pprev in *.
    case_eq (contract.has_permission_to_claim metadata); reflexivity.
  * intros n Hstate Harg.
    unfold main_get_storage.
    unfold u_max.
    unfold main.
    unfold main_option.
    unfold fail_in_main.
    unfold snd.
    unfold Pmax.
    rewrite Hstate in Pprev.
    unfold Pmax in Pprev.
    rewrite Pprev in *.
    reflexivity.
Qed.

Theorem unit_test_max : spec_max.
intros l.

unfold list_max.
rewrite <- fold_symmetric; cycle 1. 
- intros x y z.
  rewrite Nat.max_assoc.
  reflexivity.
- intros y.
  rewrite Nat.max_0_l.
  rewrite Nat.max_0_r.
  reflexivity.
- rewrite fold_get_bids.
  unfold valid_ops'.
  unfold valid_ops.
  unfold run_multiple_calls.
  unfold run_multiple_calls_.
  apply fold_invariant_aux.
  + intros state arg Hinvalid.
    rewrite invalid_is_noop_on_main; auto.
  + apply main_preserves_invariant_max.
  + compute; auto.
Qed.
