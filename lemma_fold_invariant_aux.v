Require Import List.
Import ListNotations.

Fixpoint filter_args {state : Type} {arg : Type} (valid : state -> arg -> bool) f (s : state) (l : list arg) :=
  match l with
  | [] => []
  | hd :: tl =>
    if valid s hd
    then
      hd :: (filter_args valid f (f s hd) tl)
    else
            (filter_args valid f (f s hd) tl)
  end.

Lemma fold_left_cons :
  forall (A B : Type) (f : A -> B -> A) (x : B) (l : list B) (i : A),
    fold_left f (x :: l) i = fold_left f l (f i x).
intros A B f x l i.
fold ([x] ++ l).
rewrite fold_left_app.
reflexivity.
Qed.

Definition Psnd {A B BB : Type} (P : A -> B -> Prop) a (b : BB * B) : Prop := P a (snd b).

Program Definition Psnd_rw :
  forall A B BB (P : A -> B -> Prop) a (b : BB * B),
    P a (snd b) = (Psnd P) a b := _.

Definition run_if_valid {state arg aux : Type}
                        (valid : state -> arg -> bool) f g (acc : state * aux) x :=
  if valid (fst acc) x then (f (fst acc) x, g (snd acc) x) else acc.

Lemma run_if_valid_rw :
  forall {state arg aux : Type}
         (valid : state -> arg -> bool)
         f
         g
         l
         (acc : state * aux)
         initial_value
         (initial_acc : aux)
         (Invalid_is_noop :
            forall state arg,
              valid state arg = false ->
              f state arg = state),
    fold_left g (filter_args valid f initial_value l) initial_acc
    = snd (fold_left (run_if_valid valid f g) l (initial_value, initial_acc)).
intros state arg aux valid f g l.
induction l; [ compute; auto | ].
intros acc initial_value initial_acc Invalid_is_noop.
case_eq (valid initial_value a).
- intros Hvalid.
  unfold filter_args; rewrite Hvalid; fold (filter_args valid).
  rewrite fold_left_cons.
  rewrite fold_left_cons.
  unfold run_if_valid at 2.
  unfold fst.
  unfold snd at 2.
  rewrite Hvalid.
  apply IHl; assumption.
- intros Hinvalid.
  unfold filter_args; rewrite Hinvalid; fold (filter_args valid).
  rewrite fold_left_cons.
  rewrite Invalid_is_noop; try assumption.
  unfold run_if_valid at 2.
  unfold fst.
  rewrite Hinvalid.
  apply IHl; assumption.
Qed.


Theorem fold_invariant_aux :
  forall {state : Type}
         {aux : Type}
         (initial_value : state)
         (initial_aux : aux)
         {arg : Type}
         (f : state -> arg -> state)
         (g : aux -> arg -> aux)
         (l : list arg)
         (valid : state -> arg -> bool)
         (filter_valid := filter_args valid f initial_value)
         (P : state -> aux -> Prop)

         (Invalid_is_noop :
            forall state arg,
              valid state arg = false ->
              f state arg = state)
         (Hind : forall xarg xstate xaux,
                   P xstate xaux ->
                   (*In xarg l ->*)
                   valid xstate xarg = true ->
                   P (f xstate xarg) (g xaux xarg))
         (H0 : P initial_value initial_aux),
         P (fold_left f               l  initial_value)
           (fold_left g (filter_valid l) initial_aux).
intros.
unfold filter_valid.
rewrite run_if_valid_rw; try constructor; try assumption.
rewrite Psnd_rw.
set (PP := Psnd P).
generalize dependent initial_value.
generalize dependent initial_aux.
induction l as [ | hd tl Hind' ]; [compute; auto | ].
intros.
repeat rewrite fold_left_cons.
unfold run_if_valid at 2.
unfold fst.
case_eq (valid initial_value hd).
- intros Hvalid.
  unfold snd.
  apply Hind'.
  apply Hind; try constructor; try assumption; auto.
- intros Hinvalid.
  rewrite Invalid_is_noop; try assumption.
  apply Hind'; assumption.
Qed.

(* Old versions of this lemma: *)

(*
Lemma fold_invariant :
  forall {state : Type}
         {arg : Type}
         (f : state -> arg -> state)
         (l : list arg)
         (P : state -> Prop)
         (initial_value : state)
         (Hind : forall x state, P state -> P (f state x))
         (H0 : P initial_value),
         P (fold_left f l initial_value).
intros state arg f.
apply (rev_ind
        (fun ll =>
           forall (P : state -> Prop)
                  (initial_value : state)
                  (Hind : forall x state, P state -> P (f state x))
                  (H0 : P initial_value),
                  P (fold_left f ll initial_value))).
- simpl.
  intros.
  assumption.
- intros x l0.
  simpl.
  intros Z ? ? H **.
  rewrite fold_left_app.
  unfold fold_left at 1.
  apply H.
  apply Z; assumption.
Qed.

(* ********************** *)

Definition make_gg { state arg aux }
  (f : state -> arg -> state) (g : state -> aux -> arg -> aux) :=
    fun (sa : state * aux) x => (f (fst sa) x, g (fst sa) (snd sa) x).

Theorem gg_is_like_f :
  forall {state aux arg} f g (s : state) (a : aux) (x : arg),
    fst ((make_gg f g) (s, a) x) = f s x.
intuition.
Qed.

Theorem gg_is_like_g :
  forall {state aux arg} f g (s : state) (a : aux) (x : arg),
    snd ((make_gg f g) (s, a) x) = g s a x.
intuition.
Qed.

Theorem fold_invariant_aux_ :
  forall {state : Type}
         {aux : Type}
         {arg : Type}
         (gg : (state * aux) -> arg -> (state * aux))
         (l : list arg)
         (P : state * aux -> Prop)
         (initial_value : state)
         (initial_aux : aux)
         (Hind : forall xarg xstate xaux, P (xstate, xaux) -> P (gg (xstate, xaux) xarg))
         (H0 : P (initial_value, initial_aux)),
         P (fold_left gg l (initial_value, initial_aux)).
intros.
apply fold_invariant.
- intros.
  rewrite (surjective_pairing state0).
  rewrite (surjective_pairing state0) in H.
  apply Hind.
  assumption.
- assumption.
Qed.

Theorem fold_invariant_aux :
  forall {state : Type}
         {aux : Type}
         {arg : Type}
         (f : state -> arg -> state)
         (g : state -> aux -> arg -> aux)
         (l : list arg)
         (P : state * aux -> Prop)
         (initial_value : state)
         (initial_aux : aux)
         (Hind : forall xarg xstate xaux, P (xstate, xaux) -> P ((make_gg f g) (xstate, xaux) xarg))
         (H0 : P (initial_value, initial_aux)),
         P (fold_left (make_gg f g) l (initial_value, initial_aux)).
intros.
set (gg := (make_gg f g)).
apply fold_invariant_aux_; assumption.
Qed.
*)