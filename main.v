Require Import contract.
Require Import specification.
Require Import proof_max.
Require Import proof_bids_are_less_than_sold_for_amount.
Require Import extract.