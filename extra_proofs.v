Require Import contract.
Require Import specification.
Require Import Notations.
Require Import List.
Import ListNotations.
Require Import Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Lia.

Lemma invalid_cb_after_sold :
  forall n l, valid_ops_cb (Sold n) l = [].
intros n l.
induction l; [compute; auto|].
unfold valid_ops_cb.
unfold filter_args; fold (filter_args valid_op_cb).
unfold valid_op_cb at 1.
case_eq a.
- intros n0 Ha.
  fold (valid_ops_cb).
  unfold main_get_storage.
  unfold main.
  unfold main_option.
  unfold fail_in_main.
  unfold snd.
  assumption.
- intros Ha.
  fold (valid_ops_cb).
  unfold main_get_storage.
  unfold main.
  unfold main_option.
  unfold fail_in_main.
  unfold snd.
  assumption.
Qed.

Lemma In_bids :
  forall l s amount,
  In amount (get_bids (valid_ops s l))
  -> In (Bid amount) (valid_ops s l).
intros l s amount.
set (ll := valid_ops s l).
induction ll as [| a ll IHll]; auto.
case_eq a.
- intros n Ha.
  unfold get_bids.
  unfold map_filter; fold (map_filter get_bid).
  unfold get_bid at 1.
  fold (get_bids ll).
  intros H.
  apply in_inv in H.
  destruct H.
  + apply in_inv.
    constructor.
    rewrite H.
    auto.
  + apply in_cons.
    apply IHll.
    assumption.
- intros Ha.
  unfold get_bids.
  unfold map_filter; fold (map_filter get_bid).
  unfold get_bid at 1.
  fold (get_bids ll).
  intros H.
  apply in_cons.
  apply IHll.
  assumption.
Qed.
